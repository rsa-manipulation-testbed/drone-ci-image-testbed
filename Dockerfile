FROM amarburg/drone-ci-ros-noetic:latest

RUN apt-get update && \
    apt-get install -y software-properties-common libtool && \
    rm -rf /var/lib/apt/lists/*

#RUN add-apt-repository -y ppa:lely/ppa

RUN git clone https://gitlab.com/lely_industries/lely-core.git
WORKDIR /lely-core
RUN autoreconf -i && mkdir build
WORKDIR build
RUN ../configure --disable-python2 --disable-cython --disable-python3 && make && make install && make clean
WORKDIR /lely-core/python/dcf-tools
RUN ls -al
RUN pip install .