
ROS_NOETIC_TAG=amarburg/drone-ci-image-testbed

all: build push

build: build-noetic

force: force-noetic

push:
	docker push $(ROS_NOETIC_TAG):latest 
        

## == Noetic images ==
build-noetic:
	docker build -t $(ROS_NOETIC_TAG):latest .

force-noetic:
	docker build --no-cache  -t $(ROS_NOETIC_TAG):latest .



.PHONY: build push \
			force-noetic build-noetic